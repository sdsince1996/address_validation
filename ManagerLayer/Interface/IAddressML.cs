﻿using ModelLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerLayer.Interface
{
    public interface IAddressML
    {
        public GetAddressModel SaveAddress(SetAddressModel adddressModel);
        public IEnumerable<GetAddressModel> GetAllAddress();
        public GetAddressModel GetAddByID(int addressId);
    }
}
