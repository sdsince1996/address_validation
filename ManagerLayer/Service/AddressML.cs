﻿using ManagerLayer.Interface;
using ModelLayer;
using RepositoryLayer.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerLayer.Service
{
    public class AddressML : IAddressML
    {
        private IAddressRL iaddressRL;
        public AddressML(IAddressRL iaddressRL)
        {
            this.iaddressRL = iaddressRL;
        }

        public GetAddressModel SaveAddress(SetAddressModel adddressModel)
        {
            try
            {
                return iaddressRL.SaveAddress(adddressModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<GetAddressModel> GetAllAddress()
        {
            try
            {
                return iaddressRL.GetAllAddress();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public GetAddressModel GetAddByID(int addressId)
        {
            try
            {
                return iaddressRL.GetAddByID(addressId);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
