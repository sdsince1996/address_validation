﻿using ManagerLayer.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ModelLayer;

namespace AddressValidation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        private IAddressML iaddressML;
        public AddressController(IAddressML iaddressML)
        {
            this.iaddressML = iaddressML;
        }

        [HttpPost]
        [Route("Add")]
        public IActionResult Add(SetAddressModel addModel)
        {
            try
            {
                var result = iaddressML.SaveAddress(addModel);
                if (result != null)
                {
                    return Ok(new { success = true, message = "Address Added Successfuly.", data = result});
                }
                else
                {
                    return BadRequest(new { success = false, message = "Cannot Add Address." });
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpGet]
        [Route("Get")]
        public IActionResult Get()
        {
            try
            {
                var result = iaddressML.GetAllAddress();
                if (result != null)
                {
                    return Ok(new { success = true, message = "Fetched all addresses.", data = result });
                }
                else
                {
                    return BadRequest(new { success = false, message = "Cannot fetch Addresses." });
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpGet]
        [Route("GetByID")]
        public IActionResult GetById(int addressId)
        {
            try
            {
                var result = iaddressML.GetAddByID(addressId);
                if (result != null)
                {
                    return Ok(new { success = true, message = "Fetched all address.", data = result });
                }
                else
                {
                    return BadRequest(new { success = false, message = "Cannot fetch Address." });
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
