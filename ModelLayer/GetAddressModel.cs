﻿namespace ModelLayer
{
    public class GetAddressModel
    {
        public int addressID { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string? addressLine3 { get; set; }
        public int pinCode { get; set; }
        public string city { get; set; }
        public string district { get; set; }
        public string state { get; set; }
        public string country { get; set; }
    }
}