# Address_Validation



## Task Description

1.Create a new page there will be few fields as below-
Addressline1, Addressline2, Addressline3,Pincode,City,District,State &amp; Country
2.Create a new table and columns will be same as above to store some addresses.
3.Create a new Web-API to save splitted address and fetch complete address in one-line and send that
to your page and show back fetched address.

## Input data

Single line Address
Pincode City District State Country

show address after save

Addressline1:
Addressline2:
Addressline3:
Pincode:
City:
District
State:
Country:
```

## Validations

1. If address &gt; 165 character then show some error message.
2.if address &lt; 3 words then you can use any value from city, district, state &amp; country fields in address
lines.
3. Addressline1 is mandatory, maxlength will be 55, minimum words will be 3, max words will be 5.
4. Addressline2 is mandatory, maxlength will be 55.
5. Addressline3 is optional, maxlength will be 55.

