﻿using ModelLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryLayer.Interface
{
    public interface IAddressRL
    {
        public GetAddressModel SaveAddress(SetAddressModel addModel);
        public IEnumerable<GetAddressModel> GetAllAddress();
        public GetAddressModel GetAddByID(int addressId);
    }
}
