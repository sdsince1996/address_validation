﻿using Microsoft.Extensions.Configuration;
using ModelLayer;
using Npgsql;
using RepositoryLayer.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RepositoryLayer.Service
{
    public class AddressRL : IAddressRL
    {
        public AddressRL(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        NpgsqlConnection sqlConnection;
        NpgsqlDataReader reader;
        List<GetAddressModel> addresses = new List<GetAddressModel>();
        public string address1, address2, address3;
        public GetAddressModel SplitAddress(SetAddressModel model)
        {
            int max = 0, Len1 = 0, Len2 = 0, Len3 = 0;
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex("[ ]{2,}", options);
            model.address = regex.Replace(model.address, " ");
            var array = model.address.Split(" ");
            GetAddressModel getAddressModel = new GetAddressModel();
            if (array.Length >= 5)
            {
                foreach (var item in array)
                {
                    Len1 += item.Length;
                    Len2 += item.Length;
                    Len3 += item.Length;
                    if (max < 5 && Len1 <= 55)
                    {
                        getAddressModel.addressLine1 += item + " ";
                        max++;
                        Len2 = 0;
                        Len3 = 0;
                    }
                    else if (Len2 <= 55)
                    {
                        getAddressModel.addressLine2 += item + " ";
                        getAddressModel.addressLine3 = "";
                        Len3 = 0;
                    }
                    else if (Len3 <= 55)
                    {
                        getAddressModel.addressLine3 += item + " ";
                    }
                }
            }
            else if (array.Length < 5 && array.Length >= 3)
            {
                getAddressModel.addressLine1 = model.address ;
                getAddressModel.addressLine2 = model.city + " " + model.district;
                getAddressModel.addressLine3 = "";
            }
            else if (array.Length < 3 )
            {
                getAddressModel.addressLine1 = model.address + " " + model.city + " " + model.district;
                getAddressModel.addressLine2 = model.state;
                getAddressModel.addressLine3 = "";
            }
            getAddressModel.city = model.city;
            getAddressModel.state = model.state;
            getAddressModel.district = model.district;
            getAddressModel.country = model.country;
            getAddressModel.pinCode = model.pinCode;

            return getAddressModel;
        }
        public GetAddressModel SaveAddress(SetAddressModel addModel)
        {
            sqlConnection = new NpgsqlConnection(this.Configuration.GetConnectionString("AddressDB"));
            using (sqlConnection)
                try
                {
                    var data = SplitAddress(addModel);
                    NpgsqlCommand sqlCommand = new NpgsqlCommand("CALL proc_insert(:AddressLine1, :AddressLine2, :AddressLine3, :City, :District, :State, :Country, :pincode)", sqlConnection);
                    sqlCommand.CommandType = CommandType.Text;
                    sqlConnection.Open();

                    sqlCommand.Parameters.AddWithValue("AddressLine1", DbType.String).Value = data.addressLine1;
                    sqlCommand.Parameters.AddWithValue("AddressLine2", DbType.String).Value = data.addressLine2;
                    sqlCommand.Parameters.AddWithValue("AddressLine3", DbType.String).Value = data.addressLine3;
                    sqlCommand.Parameters.AddWithValue("City", DbType.String).Value = data.city;
                    sqlCommand.Parameters.AddWithValue("District", DbType.String).Value = data.district;
                    sqlCommand.Parameters.AddWithValue("State", DbType.String).Value = data.state;
                    sqlCommand.Parameters.AddWithValue("Country", DbType.String).Value = data.country;
                    sqlCommand.Parameters.AddWithValue("pincode", DbType.String).Value = data.pinCode;

                    int result = sqlCommand.ExecuteNonQuery();
                    if (result != 0)
                    {
                        return data;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    sqlConnection.Close();
                }
        }

        public IEnumerable<GetAddressModel> GetAllAddress()
        {
            sqlConnection = new NpgsqlConnection(this.Configuration.GetConnectionString("AddressDB"));
            using (sqlConnection)
                try
                {
                    NpgsqlCommand sqlCommand = new NpgsqlCommand("select * from tbl_address;", sqlConnection);
                    sqlConnection.Open();
                    reader = sqlCommand.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            addresses.Add(new GetAddressModel()
                            {
                                addressID = Convert.ToInt32(reader["addressid"]),
                                addressLine1 = reader["AddressLine1"].ToString(),
                                addressLine2 = reader["AddressLine2"].ToString(),
                                addressLine3 = reader["AddressLine3"].ToString(),
                                city = reader["City"].ToString(),
                                district = reader["District"].ToString(),
                                state = reader["State"].ToString(),
                                country = reader["Country"].ToString(),
                                pinCode = Convert.ToInt32(reader["pincode"])
                            });
                        }
                        return addresses;
                    }
                    return null;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    sqlConnection.Close();
                }
        }

        public GetAddressModel GetAddByID(int addressId)
        {
            sqlConnection = new NpgsqlConnection(this.Configuration.GetConnectionString("AddressDB"));
            using (sqlConnection)
                try
                {
                    NpgsqlCommand sqlCommand = new NpgsqlCommand("select * from tbl_address where addressid =" + addressId, sqlConnection);
                    sqlConnection.Open();
                    reader = sqlCommand.ExecuteReader();
                   
                    GetAddressModel model = new GetAddressModel();
                    while (reader.Read())
                    {
                        model.addressID = Convert.ToInt32(reader["addressid"]);
                        model.addressLine1 = reader["AddressLine1"].ToString();
                        model.addressLine2 = reader["AddressLine2"].ToString();
                        model.addressLine3 = reader["AddressLine3"].ToString();
                        model.city = reader["City"].ToString();
                        model.district = reader["District"].ToString();
                        model.state = reader["State"].ToString();
                        model.country = reader["Country"].ToString();
                        model.pinCode = Convert.ToInt32(reader["pincode"]);
                            
                    }
                    return model;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    sqlConnection.Close();
                }
        }
    }
}
